<?php

class WebServicesController extends AppController{

	private $_climas = [
		'Soleado',
		'Lluvioso',
		'Nublado',
		'Parcialmente Nublado',
		'Nevazon'
	];
	
	private $_estaciones = [
		'Verano',
		'Otoño',
		'Primavera',
		'Invierno'
	];
	
	private $_horario = [
		'Mañana',
		'Tarde',
		'Noche',
		'Madrugada'
	];
	
	private function validaWebService(){
		$this->layout = null;
		$this->autoRender = false;
		
		if(!$this->request->is('post')){
			die('error');
		}
	}
	
	public function getSeason(){
		$this->validaWebService();
		
		App::import('Model','Estacion');
		$this->Estacion = new Estacion();
		
		$usadas = $this->Estacion->find('list',[
					'recursive' => -1,
					'fields' => ['indice','valor']
		]);
		
		# Si ya se usaron todas, comenzamos de nuevo.
		if(count($usadas) == count($this->_estaciones)){
			$this->Estacion->deleteAll(['id >' => 0]);
			
			$indice = rand(0,3);
		} else {
			do{
				$indice = rand(0,3);
			}while(in_array($indice,array_keys($usadas)));
		}
		
		$this->Estacion->save(['indice' => $indice,'valor' => $this->_estaciones[$indice]]);
		
		echo json_encode([
			'valor' => $this->_estaciones[$indice]
		]);
		exit();
	}
	
	public function getWeather(){
		$this->validaWebService();
		
		App::import('Model','Clima');
		$this->Clima = new Clima();
		
		$usadas = $this->Clima->find('list',[
					'recursive' => -1,
					'fields' => ['indice','valor']
		]);
		
		# Si ya se usaron todas, comenzamos de nuevo.
		if(count($usadas) == count($this->_climas)){
			$this->Clima->deleteAll(['id >' => 0]);
			
			$indice = rand(0,4);
		} else {
			do{
				$indice = rand(0,4);
			}while(in_array($indice,array_keys($usadas)));
		}
		
		$this->Clima->save(['indice' => $indice,'valor' => $this->_climas[$indice]]);
		
		echo json_encode([
			'valor' => $this->_climas[$indice]
		]);
		exit();
	}
	
	public function getHorario(){
		$this->validaWebService();
		
		App::import('Model','Horario');
		$this->Horario = new Horario();
		
		$usadas = $this->Horario->find('list',[
					'recursive' => -1,
					'fields' => ['indice','valor']
		]);
		
		# Si ya se usaron todas, comenzamos de nuevo.
		if(count($usadas) == count($this->_horario)){
			$this->Horario->deleteAll(['id >' => 0]);
			
			$indice = rand(0,3);
		} else {
			do{
				$indice = rand(0,3);
			}while(in_array($indice,array_keys($usadas)));
		}
		
		$this->Horario->save(['indice' => $indice,'valor' => $this->_horario[$indice]]);
		
		echo json_encode([
			'valor' => $this->_horario[$indice]
		]);
		exit();
	}

	public function getTemperature(){
		$this->validaWebService();
		
		App::import('Model','Temperatura');
		$this->Temperatura = new Temperatura();
		
		$usadas = $this->Temperatura->find('list',[
					'recursive' => -1,
					'fields' => ['id','valor']
		]);
		
		# Si ya se usaron todas, comenzamos de nuevo.
		if(count($usadas) == 40){
			$this->Temperatura->deleteAll(['id >' => 0]);
			
			$temperatura = rand(-5,35);
		} else {
			do{
				$temperatura = rand(-5,35);
			}while(in_array($temperatura,$usadas));
		}
		
		$this->Temperatura->save(['valor' => $temperatura]);
		
		echo json_encode([
			'valor' => $temperatura
		]);
		exit();
	}
	
	public function getHumidity(){
		$this->validaWebService();
		
		App::import('Model','Humedad');
		$this->Humedad = new Humedad();
		
		$usadas = $this->Humedad->find('list',[
					'recursive' => -1,
					'fields' => ['id','valor']
		]);
		
		# Si ya se usaron todas, comenzamos de nuevo.
		if(count($usadas) == 100){
			$this->Humedad->deleteAll(['id >' => 0]);
			
			$humedad = rand(0,100);
		} else {
			do{
				$humedad = rand(0,100);
			}while(in_array($humedad,$usadas));
		}
		
		$this->Humedad->save(['valor' => $humedad]);
		
		echo json_encode([
			'valor' => $humedad
		]);
		exit();
	}
}